/* eslint-disable no-unused-vars */
import Swiper from 'swiper';

function swiper() {
    // свайпер слайдер для сектопов
    // eslint-disable-next-line no-shadow
    const swiper = new Swiper('.js-comand-slider', {
        spaceBetween: 0,
        freeMode: true,
        watchOverflow: true,
        slidesPerView: 'auto',
        slideClass: 'comand-item',
        wrapperClass: 'comand-container',
        slidePrevClass: 'comand-item-btn__slide--prev',
        slideNextClass: 'comand-item-btn__slide--next',
        slideActiveClass: 'comand-item--active',
        navigation: {
            nextEl: '.comand-slider-btn--next',
            prevEl: '.comand-slider-btn--prev',
        }
    });
}

if ($('.js-comand-slider')[0]) {
    swiper();
}
