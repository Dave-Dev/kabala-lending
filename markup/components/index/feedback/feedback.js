/* eslint-disable no-unused-vars */
import Swiper from 'swiper';

function swiperF() {
    // свайпер слайдер для сектопов
    // eslint-disable-next-line no-shadow
    const swiperF = new Swiper('.js-feedback-slider', {
        loop: true,
        spaceBetween: 0,
        slidesPerView: 1,
        // autoHeight: true,
        slideClass: 'feedback-article',
        wrapperClass: 'feedback-article-wrapper',
        slidePrevClass: 'feedback-article--prev',
        slideNextClass: 'feedback-article--next',
        slideActiveClass: 'feedback-article--active',
        navigation: {
            nextEl: '.feedback-slider-btn--next',
            prevEl: '.feedback-slider-btn--prev',
        }
    });
}

if ($('.js-feedback-slider')[0]) {
    swiperF();
}

$(() => {
    $('.js-feedback-btn-more').on('click', function () {
        $(this).hide().siblings('.feedback-article-body').addClass('active');
    });
})