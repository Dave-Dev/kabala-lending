/* eslint-disable no-unused-vars */
import Swiper from 'swiper';

function swiperH() {
    // свайпер слайдер для сектопов
    // eslint-disable-next-line no-shadow
    const swiperH = new Swiper('.js-from-slider', {
        spaceBetween: 0,
        slidesPerView: 1,
        effect: 'fade',
        loop: true,
        slideClass: 'from-history-slider-item',
        wrapperClass: 'from-history-slider-wrapper',
        slidePrevClass: 'from-history-slider-item--prev',
        slideNextClass: 'from-history-slider-item--next',
        slideActiveClass: 'from-history-slider-item--active',
        navigation: {
            nextEl: '.from-history-slider--next',
            prevEl: '.from-history-slider--prev',
        },
        on: {
            init: () => {
                $('.from-history-slider-item-img').attr('src', $('.js-from-slider').find('.from-history-slider-item--active').data('img'));
            },
            slideChange: () => {
                $('.from-history-slider-item-img').attr('src', $('.js-from-slider').find('.from-history-slider-item--active').data('img'));
            }
        }
    });
}

if ($('.js-from-slider')[0]) {
    swiperH();
}
