$(() => {
    $(window).on('resize scroll', function () {
        $('.js-article-img').each(function () {
            if (
                $(this)[0] &&
                $(this).isInViewport() &&
                !$(this).hasClass('inited')
            ) {
                $(this).addClass('inited');
            }
        });
    });
});
