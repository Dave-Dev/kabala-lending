'use strict';

import $ from 'jquery';
window.$ = window.jQuery = $;

import './plugins/isInViewPort';

import '@fancyapps/fancybox';
// маска для телефона
import Cleave from 'cleave.js';
require('cleave.js/dist/addons/cleave-phone.ru.js');

import 'components/index/top/top';
import 'components/index/comand/comand';
import 'components/index/middle/from';
import 'components/index/feedback/feedback';

// убираем автофокус с элементов
$.fancybox.defaults.autoFocus = false;
$.fancybox.defaults.backFocus = false;
$.fancybox.defaults.lang = 'ru';
$.fancybox.defaults.i18n = {
    ru: {
        CLOSE: 'Закрыть',
        NEXT: 'Следущее изображение',
        PREV: 'Предыдущее изображение',
        ERROR: 'Загрузка контента не удалась. <br/> Пожалуйста повторите позднее.',
        PLAY_START: 'Запустить слайдшоу',
        PLAY_STOP: 'Остановить слайдшоу',
        FULL_SCREEN: 'На полный экран',
        THUMBS: 'Миниатюры',
        DOWNLOAD: 'Скачать',
        SHARE: 'Поделиться',
        ZOOM: 'Увеличить'
    }
};

$(() => {
    // запуск маски для телефона
    $('body').on('focus', '.js-mask-phone', function () {
        // если поле не заполнено то подставляем начало номера
        let val = $(this).val();
        if (!val) {
            $(this).val('+7 ');
        } else {
            $(this).val(val);
        }
        $('.js-mask-phone').toArray().forEach(function (field) {
            new Cleave(field, {
                delimiters: [' '],
                blocks: [2, 3, 3, 2, 2]
            });
        });
    });
    // убираем с поля для телефона любые символы кроме цифр
    $('.js-mask-phone').on('keydown', function (e) {
        if (e.key.length == 1 && e.key.match(/[^0-9'".]/)) {
            return false;
        }
    });
    // если в поле есть символы то ставим класс
    $('input').on('blur', function () {
        if ($(this).val().length >= 1) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    // плавный скролл при клике
    $('.js-btn-down[href^="#"]').click(function () {
        $('html, body').animate({
            scrollTop: $('[id="' + $.attr(this, 'href').substr(1) + '"]').offset().top
        }, 500);
        return false;
    });
});
